import  axios from 'axios';


const instance = axios.create({
    baseURL: 'https://homework63-563e0.firebaseio.com/'

});

export default instance;