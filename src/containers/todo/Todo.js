import React, { Component } from 'react';
import './Todo.css';
import Header from "../../components/todo/header";
import TaskInput from "../../components/todo/taskInput";
import TaskItem from "../../components/todo/task";
import axios from '../../axios';
import Spinner from "../../components/UI/Spinner/Spinner";

class Todo extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentTask: '',
            tasks: [],
            loading: false
        };

        this.addTask = this.addTask.bind(this);
        this.removeTask = this.removeTask.bind(this);
    }

    componentDidMount() {
        this.getTask();

    };
    getTask =()=> {
        this.setState({loading: true});
        axios.get('/todo.json').then((response) => {
            response.data ?
            this.setState({tasks: response.data, loading: false}) : this.setState({tasks: [], loading: false});
        })
    };

    handleChange = (e) => {
        this.setState({currentTask: e.target.value});
    };

    addTask(e) {
        e.preventDefault();
        this.setState({loading: true});
        axios.post('/todo.json', {currentTask: this.state.currentTask}).then(() => {

            this.getTask();
        }).finally(()=> {
            this.setState({loading: false});

        });

    }

    removeTask(taskId) {
        axios.delete(`/todo/${taskId}.json`).then(() => {
            this.getTask();
        })

    }

    render() {
        let tasksId = Object.keys(this.state.tasks);
        return (
            <div className="App">
                {this.state.loading ? <Spinner /> : null}
                <div className="task-wrapper">
                    <Header />
                    <TaskInput changeCurrentTask = {this.handleChange}
                                currentTask={this.state.currentTask}
                               click={this.addTask}
                    />

                    <ul>
                        {
                            tasksId.map((taskId) => {
                                return <TaskItem task={this.state.tasks[taskId]} key={taskId}
                                                 remove={() => this.removeTask(taskId)}/>
                            })
                        }
                    </ul>
                </div>
            </div>
        )
    }
}


export default Todo;