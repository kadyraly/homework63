import React, {Component} from 'react';

import './Cinema.css';

import FilmPreview from "../../components/movies/FilmPreview/FilmPreview";
import WatchFilmControl from "../../components/movies/WatchFilmControl/WatchFilmControl";
import axios from '../../axios';
import Spinner from "../../components/UI/Spinner/Spinner";



class Cinema extends Component {
    state = {
        films: [],
        text: '',
        loading: false

    };

    componentDidMount() {
        this.getMovie();
        };
    getMovie=()=> {
        this.setState({loading: true});
        axios.get('/cinema.json').then((response) => {
            response.data ?
                this.setState({films: response.data, loading: false}) : this.setState({films: [], loading: false});

        })
    };

        handleFilmChange = (e) => {
        this.setState({text: e.target.value});
    };


    handleChange=(e, id) => {
        const films = [...this.state.films];
        const index = films.findIndex((film) => film.id === id);
        const film = {...this.state.films[index]};
        film.text = e.target.value;
        films[index] = film;
        this.setState({films: films})
    };





    addFilms = (e) => {
        e.preventDefault();
        this.setState({loading: true});

        axios.post('/cinema.json', {text: this.state.text}).then(() => {
            this.getMovie();
        }).finally(()=> {
            this.setState({loading: false});

        });
    };


    removeFilms = (filmId) => {

         axios.delete(`/cinema/${filmId}.json`).then(() => {
             this.getMovie();
         })

    };




    render() {

        return (
            <div className="Cinema">
                {this.state.loading ? <Spinner /> : null}
                <WatchFilmControl
                    text={this.state.text}
                    textChange={this.handleFilmChange}
                    click={this.addFilms}
                />
                <FilmPreview films={this.state.films} change={this.handleChange} remove={this.removeFilms}/>
            </div>
        );
    }
}

export default Cinema;