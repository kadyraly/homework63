import React, {Component } from 'react';

import './App.css';
import Todo from "./containers/todo/Todo";
import Cinema from "./containers/movies/Cinema";
import Header from "./components/Header/Header";
import { Switch, Route} from "react-router-dom";
import Home from "./components/Home/Home";
import Footer from "./components/Footer/Footer";


class App extends Component {
  render() {
    return (
      <div className="list">
          <Header />
          <Switch>
              <Route path="/"  exact component={Home} />
              <Route path="/about" component={Todo} />
              <Route path="/contact" component={Cinema} />
          </Switch>
          <Footer />
      </div>
    );
  }
}

export default App;
