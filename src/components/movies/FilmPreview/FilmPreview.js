import React from 'react';
import './FilmPreview.css';
import Movie from "../Movie/Movie";

const FilmPreview =(props) => {

    let filmsId = Object.keys(props.films);

    return (
        <div className="FilmPreview">
            <p>To watch list:</p>
            {filmsId.map((filmId) => {
                return <Movie
                    change={(e) => props.change(e, filmId)}
                    value={props.films[filmId].text} key={filmId}
                    remove={() => props.remove(filmId)}/>
                // return <div><input key={film.id} value={film.text} onChange={(e) => props.change(e, film.id)} /><span onClick={() => props.remove(film.id)}>X</span></div>
            })}

        </div>
    )
};

export default FilmPreview;