import React from 'react';
import  './WatchFilmControl.css';

const WatchFilmControl =(props) => {
    return (
        <div>
            <form>
                <input className="inputText" type="text" placeholder="Film name" value={props.text} onChange={props.textChange} />

                <button onClick ={props.click} className="WatchFilmControl-btn">
                    Add
                </button>
            </form>
        </div>
    );

};
export default  WatchFilmControl