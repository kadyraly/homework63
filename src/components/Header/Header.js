import React from 'react';
import './Header.css';
import {NavLink} from "react-router-dom";

const Header =()=> {
    return (
        <div className="Header">
            <ul>
                <li><NavLink activeClassName="selected" exact to="/">Home</NavLink></li>
                <li><NavLink activeClassName="selected" to="/about">Task</NavLink></li>
                <li><NavLink activeClassName="selected" to="/contact">Movie</NavLink></li>
            </ul>
        </div>
    )
};

export  default Header;